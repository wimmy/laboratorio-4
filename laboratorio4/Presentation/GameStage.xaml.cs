using System.ComponentModel;
using laboratorio4.ViewModels;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media.Imaging;
using laboratorio4.Models;
using Microsoft.UI.Xaml.Shapes;
using Microsoft.UI;

namespace laboratorio4.Presentation;
public sealed partial class GameStage : Page
{
    private ViewModelGameStage vm;

    public GameStage()
    {
        this.InitializeComponent();
        vm = (ViewModelGameStage)DataContext;

        vm.PropertyChanged += PropertyChanged;
    }

    protected override void OnNavigatedTo(NavigationEventArgs e)
    {
        base.OnNavigatedTo(e);
    }

    private void PropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == nameof(ViewModelGameStage.Enemies))
        {
            DrawEnemies(MainCanvas, vm.Enemies);
        }
    }

    private void Canvas_KeyDown(object sender, Microsoft.UI.Xaml.Input.KeyRoutedEventArgs e)
    {
        if (e.Key == Windows.System.VirtualKey.Right)
        {
            vm.MoveLeftCommand.Execute(e);
        }
        else if (e.Key == Windows.System.VirtualKey.Up)
        {
            vm.MoveTopCommand.Execute(e);
        }
    }

    private void Page_Loaded(object sender, RoutedEventArgs e)
    {
        MainCanvas.Focus(FocusState.Programmatic);
        StartTimer();
        DrawEnemies(MainCanvas, vm.Enemies);
    }

    public void StartTimer()
    {
        DispatcherTimer timer = new DispatcherTimer();
        timer.Interval = TimeSpan.FromMilliseconds(100);
        timer.Tick += Timer_Tick;
        timer.Start();
    }

    private void Timer_Tick(object? sender, object e)
    {
        this.vm.MoveEnemiesCommand.Execute(null);
    }
    public void DrawEnemies(Canvas canvas, List<Enemy> enemies)
    {
        var items = canvas.Children.Where(x => x.GetType() != typeof(Image)).ToList();
        foreach (var item in items)
        {
            canvas.Children.Remove(item);
        }

        foreach (var enemy in enemies)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 50;
            rect.Height = 50;
            rect.Fill = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri("ms-appx:C:\\Users\\Wimmy\\Source\\Repos\\laboratorio-4\\laboratorio4\\Assets\\Ships\\ship_2.png"))
            };

            Canvas.SetLeft(rect, enemy.PositionLeft);
            Canvas.SetTop(rect, enemy.PositionTop);

            canvas.Children.Add(rect);
            canvas.InvalidateArrange();
        }
    }
}
