using laboratorio4.ViewModels;

namespace laboratorio4.Presentation;

public sealed partial class MainPage : Page
{
    private MainViewModel vm;
    public MainPage()
    {
        this.InitializeComponent();
        vm = (MainViewModel)DataContext;

    }
    private void btnGoToPage_Click(object sender, RoutedEventArgs e)
    {
        Frame.Navigate(typeof(GameStage));
    }
}
