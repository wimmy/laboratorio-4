using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio4.Models;
public partial class Player : ObservableObject
{
    [ObservableProperty]
    private int positionTop;

    [ObservableProperty]
    private int positionLeft;

    public Player()
    {
        this.positionTop = 450;
        this.PositionLeft = 10;
    }

    public void MoveRight()
    {
        PositionLeft += 10;
    }

    public void MoveTop()
    {
        PositionTop -= 10;
    }
}
