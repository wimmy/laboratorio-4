using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio4.Models;
public partial class Enemy : ObservableObject
{
    [ObservableProperty]
    public int positionTop;

    [ObservableProperty]
    public int positionLeft;

    public Enemy(int positionTop, int positionLeft)
    {
        this.PositionTop = positionTop;
        PositionLeft = positionLeft;
    }
}
