using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio4.Models;

public class Game
{
    public Player Player { get; set; }
    public List<Enemy> Enemies { get; set; }

    public Game()
    {
        Player = new Player();
        Enemies =
        [
            new Enemy(50, 0),
            new Enemy(50, 80),
            new Enemy(50, 150),
            new Enemy(50, 230),
        ];
    }

    public void MovePlayerRight()
    {
        Player.MoveRight();
    }

    public void MovePlayerTop()
    {
        Player.MoveTop();
    }

    public void MoveEnemies()
    {
        foreach (var item in Enemies)
        {
            item.PositionLeft += 10;
        }
    }
}
