using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laboratorio4.Utils;
internal class BindablePoint   : INotifyPropertyChanged
{
        private double _x;
public double X
{
    get { return _x; }
    set
    {
        if (_x != value)
        {
            _x = value;
            OnPropertyChanged("X");
        }
    }
}

private double _y;
public double Y
{
    get { return _y; }
    set
    {
        if (_y != value)
        {
            _y = value;
            OnPropertyChanged("Y");
        }
    }
}

public BindablePoint(double x, double y)
{
    _x = x;
    _y = y;
}

public event PropertyChangedEventHandler PropertyChanged;

protected void OnPropertyChanged(string propertyName)
{
    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}
}
