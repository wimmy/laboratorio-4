using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using laboratorio4.Models;


namespace laboratorio4.ViewModels;
partial class ViewModelGameStage : ObservableObject

{
    private Game game;

    public ViewModelGameStage()
    {
        game = new Game();
    }

    public int PlayerTop
    {
        get { return game.Player.PositionTop; }
        set
        {
            game.Player.PositionTop = value;
            OnPropertyChanged(nameof(PlayerTop));
        }
    }

    public int PlayerLeft
    {
        get { return game.Player.PositionLeft; }
        set
        {
            game.Player.PositionLeft = value;
            OnPropertyChanged(nameof(PlayerLeft));
        }
    }
    public List<Enemy> Enemies
    {
        get { return game.Enemies; }
    }
    [RelayCommand]
    private void MoveLeft()
    {
        PlayerLeft += 10;
    }
    [RelayCommand]
    private void MoveTop()
    {
        PlayerTop -= 10;
    }
    [RelayCommand]
    private void MoveEnemies()
    {
        game.MoveEnemies();
        OnPropertyChanged(nameof(Enemies));
    }
}

